module github.com/yt8956gh/testGoModule

go 1.14

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.0
	github.com/chenht1998/testA v1.0.4
	github.com/jordan0210/releaseGoModTest v1.0.3
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20210113181707-4bcb84eeeb78 // indirect
)
